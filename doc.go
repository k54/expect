/* Package expect implements a simple testing enhancer.

It is not meant to replace testing, but only to make it better at expressiveness.

For example, you can use any of the checks in any testing subfunction without passing any
variable to it.

You could also technically use this package as an assertions package if you write
custom recovery (expectlib.AsInternalError(recover()) would be a good start).

This package is also meant to be quite simple and not have any dependency, so that
you can audit it easily.

You may observe that there are very few checks implemented compared to other testing libraries.
This is completely normal, and you are encouraged to write your own
domain-specific checks, as all functions are implemented on top of expectlib,
which you can also use.

Most simple checks can be written as a form of expect.True or expect.False.

This lib will have a v2 once parametric polymorphism (aka generics) are live and stable.

Checks planned for generics:
- Equal: func[T comparable](a, b T) { a == b ...}
- NotEqual: func[T comparable](a, b T) { a != b ...}
- ErrorAs: func[T error](err error, target T) {var t T; errors.As(err, &t) ...}
- better implementations for almost all checks (no more interface{})
- some way to set/unset stubs

Implementation details:

Makes heavy use of reflection, panic/recover and runtime calls, so not a the fastest library.
It is still fast enough for testing purposes, and is faster than most easy errors packages.

The reason it's still quite fast is how the call stack is recorded: see expectlib for more info.

It panics with a expectlib.InternalError for signalling a failure (and records
the call stack to the InternalError). Cleanup (if it is setup for current test)
then recovers, checks if panicked was an InternalError, and repanics otherwise.

If it was an InternalError, logs a nice call stack and failure info, and marks the
test as having failed.
*/
package expect
