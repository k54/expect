package expect

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"testing"

	"gitlab.com/k54/expect/expectlib"
)

var stubTFailNow = func(tb testing.TB) { tb.Helper(); tb.FailNow() }

// Cleanup is a function that may be called WITH A DEFER at the start of each of your tests.
//
// Your tests will work exactly the same without setting this up, but the fails
// will look horrible.
//
// This is one of the most complex function of this package, with StubUnstub,.
func Cleanup(tb testing.TB) {
	tb.Helper()
	// new(testing.B).
	v := recover()
	if v == nil {
		return
	}
	ie, ok := expectlib.AsInternalError(v)
	if !ok {
		panic(v)
	}

	// no stack to show, but we still had a problem
	if len(ie.Stack) == 0 {
		tb.Logf("%s", ie.Msg)
		stubTFailNow(tb)
	}

	frames := runtime.CallersFrames(ie.Stack)

	var done bool
	for {
		frame, more := frames.Next()
		// force break if we are at last frame, it should never happen though
		if !more {
			break
		}
		// we arrived at testing package, end of user frames
		if frame.Function == "testing.tRunner" {
			break
		}

		// file name after last slash
		file := frame.File
		i := strings.LastIndexByte(file, '/')
		file = file[i+1:]

		// kinda hacked but we overwrite the testing package's own file info logging
		if !done {
			// first frame, show message
			tb.Logf("\r    %s:%d: %v", file, frame.Line, ie.Msg)
			done = true
		} else {
			// not first frame, show caller of previous frame
			tb.Logf("\r        called by: %s (%s:%d)", frame.Function, file, frame.Line)
		}
	}
	stubTFailNow(tb)
}

// Nil is basically the expect verion of ``if v == nil''.
//
// It will fail on non-nillable types, such as int.
//
// Equivalent to ``True(v == nil)''.
//
// Prefer using ``True(v == nil)'' when possible, as it has better compiler support
// (waiting for generics).
func Nil(v interface{}) {
	if v == nil {
		return
	}
	rv := reflect.ValueOf(v)
	switch rv.Kind() {
	// nilable Kinds
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.Ptr, reflect.Slice:
		if !rv.IsNil() {
			expectlib.InternalFailf(0, "%v should be nil", v)
		}
	default:
		expectlib.InternalFailf(0, "%v of type %s cannot be nil", v, rv.Kind())
	}
}

// NotNil is basically the expect verion of ``if v != nil''.
//
// It will not fail on non-nillable types, such as int.
//
// Equivalent to ``True(v != nil)''.
func NotNil(v interface{}) {
	if v == nil {
		expectlib.InternalFail(0, "value should not be nil")
	}
	rv := reflect.ValueOf(v)
	switch rv.Kind() {
	// nilable Kinds
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.Ptr, reflect.Slice:
		if rv.IsNil() {
			expectlib.InternalFail(0, "value should be not nil")
		}
	default:
		return // non-nillable types will not be nil
	}
}

// DeepEqual is basically the expect verion of reflect.DeepEqual(a, b).
func DeepEqual(a, b interface{}) {
	if !reflect.DeepEqual(a, b) {
		expectlib.InternalFailf(0, "%[1]T(%+[1]v) and %[2]T(%+[2]v) should be equal", a, b)
	}
}

// NotDeepEqual is basically the expect verion of !reflect.DeepEqual(a, b).
func NotDeepEqual(a, b interface{}) {
	if reflect.DeepEqual(a, b) {
		expectlib.InternalFailf(0, "%[1]T(%+[1]v) and %[2]T(%+[2]v) should be different", a, b)
	}
}

// Zero is basically the expect verion of reflect.ValueOf(v).IsZero(), but a bit safer.
//
// Will fail on nil (makes no sense).
func Zero(v interface{}) {
	rv := reflect.ValueOf(v)
	if !rv.IsValid() || !rv.IsZero() {
		expectlib.InternalFailf(0, "%[1]T(%+[1]v) is not the zero value of its type", v)
	}
}

// NotZero is basically the expect verion of !reflect.ValueOf(v).IsZero(), but a bit safer.
//
// Will fail on nil (makes no sense).
func NotZero(v interface{}) {
	rv := reflect.ValueOf(v)
	if !rv.IsValid() || rv.IsZero() {
		expectlib.InternalFailf(0, "%[1]T(%+[1]v) is the zero value of its type", v)
	}
}

// True is basically the expect version of ``if !cond { t.Fatal(...) }''.
func True(cond bool) {
	if !cond {
		expectlib.InternalFailf(0, "failed condition, expected true")
	}
}

// If is basically the expect version of ``if cond { t.Fatalf(msg, a...) }''.
func If(cond bool, msg string, a ...interface{}) {
	if cond {
		expectlib.InternalFailf(0, fmt.Sprintf(msg, a...))
	}
}

// False is basically the expect version of ``if cond { t.Fatal(...) }''.
func False(cond bool) {
	if cond {
		expectlib.InternalFailf(0, "failed condition, expected false")
	}
}

// Panic calls fn, fails if it did not panic, and continues if it panicked.
//
// Recovers, and repanics if is an InternalError so that failures are transparent.
func Panic(fn func()) {
	defer func() {
		v := recover()
		// rethrows if v is an internal error
		if v != nil {
			_, ok := expectlib.AsInternalError(v)
			if ok {
				panic(v)
			}
			// panic happened as expected
			return
		}
		expectlib.InternalFail(0, "test was supposed to panic")
	}()
	fn()
}

// Failure runs fn, and fails if fn did not fail. It recovers the failure that should happen
// and continues on if it happened.
//
// Recovers, and repanics if not an InternalError so that panics are transparent.
func Failure(fn func()) {
	defer func() {
		v := recover()
		if v != nil {
			_, ok := expectlib.AsInternalError(v)
			if ok {
				return
			}
		}
		expectlib.InternalFail(0, "test was supposed to fail")
	}()
	fn()
}

// StubUnstub is an experimental auto-stubbing utility
//
// Call like: defer StubUnstub(t, &osExitStub)()
//
// It will create a stub and swap it in, and then on deferred call swap the old stub back
// and t.Fatal() if stub was not called.
func StubUnstub(t *testing.T, toReplace interface{}) func() {
	t.Helper()
	rv := reflect.ValueOf(toReplace)
	if rv.Kind() != reflect.Ptr || rv.Elem().Kind() != reflect.Func {
		panic("stub to replace must be a pointer to a function")
	}
	rv = rv.Elem()

	called := false
	fn := reflect.MakeFunc(rv.Type(), func(args []reflect.Value) (results []reflect.Value) {
		called = true
		return
	})

	kept := reflect.New(rv.Type()).Elem()
	kept.Set(rv)
	rv.Set(fn)
	return func() {
		rv.Set(kept)
		if !called {
			t.Log("stub not called")
			stubTFailNow(t)
		}
	}
}
