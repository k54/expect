package expect

import (
	"runtime"
	"sync"
	"testing"

	"gitlab.com/k54/expect/expectlib"
)

// not the best test, but at least i get coverage
// VERY hard to test t.FailNow, especially because it uses runtime.Goexit.
func TestStubTFailNow(t *testing.T) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer func() {
			wg.Done()
		}()
		t2 := &testing.T{}
		stubTFailNow(t2)
		t.Log("kept xecutiong after stubTFailNow()")
		t.Fail()
	}()
	wg.Wait()
}

func TestCleanup(t *testing.T) {
	t.Run("repanic", func(t *testing.T) {
		defer func() {
			v := recover()
			t.Log(v)
		}()
		defer Cleanup(t)
		panic(42)
	})

	t.Run("catch", func(t *testing.T) {
		defer StubUnstub(t, &stubTFailNow)()
		defer Cleanup(t)
		True(false)
	})

	t.Run("catch/deep", func(t *testing.T) {
		defer StubUnstub(t, &stubTFailNow)()
		defer Cleanup(t)
		func() {
			True(false)
		}()
	})

	t.Run("force0stack", func(t *testing.T) {
		defer StubUnstub(t, &stubTFailNow)()
		defer Cleanup(t)
		panic(expectlib.InternalError{Msg: "manually created error"})
	})
	t.Run("force1stack", func(t *testing.T) {
		defer StubUnstub(t, &stubTFailNow)()
		defer Cleanup(t)
		rpc := make([]uintptr, 64)
		n := runtime.Callers(0, rpc)
		panic(expectlib.InternalError{Msg: "manually created error", Stack: rpc[n-1 : n]})
	})
}

func TestStubUnstub(t *testing.T) {
	t.Run("bad_arg", func(t *testing.T) {
		Panic(func() {
			theStub := 42
			StubUnstub(t, &theStub)
		})
	})
	t.Run("call", func(t *testing.T) {
		theStub := func() {}
		defer StubUnstub(t, &theStub)()
		theStub()
	})
	t.Run("dont_call", func(t *testing.T) {
		failed := false
		old := stubTFailNow
		stubTFailNow = func(tb testing.TB) { tb.Helper(); failed = true }
		defer func() {
			stubTFailNow = old
			if !failed {
				t.Fatal("test should have failed")
			}
		}()
		theStub := func() {}
		defer StubUnstub(t, &theStub)()
	})
}
