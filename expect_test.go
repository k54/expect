package expect_test

import (
	"errors"
	"io"
	"testing"

	"gitlab.com/k54/expect"
)

func ExampleTrue() {
	var err error
	// err = lib.DoSomething()
	expect.True(errors.Is(err, io.EOF))
	// It could have been:
	// expect.ErrorIs(err, io.EOF)
	// but this would make for a very bloated library and only save a few characters
}

func TestT(t *testing.T) {
	defer expect.Cleanup(t)
	var err error
	expect.Nil(err)
}

func TestTManual(t *testing.T) {
	var err error
	if err != nil {
		t.Fatal(err)
	}
}

func TestFailure(t *testing.T) {
	defer expect.Cleanup(t)
	expect.Failure(func() { expect.True(false) })
	expect.Failure(func() {
		expect.Failure(func() { expect.True(true) })
	})
}

func TestPanic(t *testing.T) {
	defer expect.Cleanup(t)
	expect.Panic(func() { panic("i should panic") })
	expect.Failure(func() {
		expect.Panic(func() { expect.True(false) })
	})
}

func TestNoPanic(t *testing.T) {
	defer expect.Cleanup(t)
	expect.Failure(func() {
		expect.Panic(func() {})
	})
}

func TestDeepEqual(t *testing.T) {
	defer expect.Cleanup(t)
	// not a lot of test cases, because it's only a thin wrapper for reflect.DeepEqual
	expect.DeepEqual(42, 42)
	expect.Failure(func() { expect.DeepEqual(42, 43) })
}

func TestNotDeepEqual(t *testing.T) {
	defer expect.Cleanup(t)
	// not a lot of test cases, because it's only a thin wrapper for reflect.DeepEqual
	expect.NotDeepEqual(42, uint(42))
	expect.NotDeepEqual(42, 43)
	expect.NotDeepEqual(42, 42.0)
	expect.NotDeepEqual(42, "the answer to life")
	expect.Failure(func() { expect.NotDeepEqual(42, 42) })
}

func TestTrue(t *testing.T) {
	defer expect.Cleanup(t)
	// what a long test
	expect.True(true)
	expect.Failure(func() { expect.True(false) })
}

func TestFalse(t *testing.T) {
	defer expect.Cleanup(t)
	// what a long test
	expect.False(false)
	expect.Failure(func() { expect.False(true) })
}

func TestIf(t *testing.T) {
	defer expect.Cleanup(t)
	// what a long test
	expect.If(false, "bad stuff :(")
	expect.Failure(func() { expect.If(true, "bad stuff :(") })
}

// func TestFail(t *testing.T) {
// 	defer expect.Setup(t)
// 	recurse(5)
// }

// func recurse(n int) {
// 	if n <= 0 {
// 		expectlib.Fail("recursion too deep")
// 	}
// 	recurse(n - 1)
// }

var (
	zvChan chan int
	zvFunc func()
	// ZvInterface // cannot get one.
	zvMap   map[string]int
	zvPtr   *int
	zvSlice []int
)

func TestNotNil(t *testing.T) {
	defer expect.Cleanup(t)

	expect.Failure(func() { expect.NotNil(nil) })

	// nillable types
	expect.NotNil(make(chan int))
	expect.Failure(func() { expect.NotNil(zvChan) })
	expect.NotNil(func() {})
	expect.Failure(func() { expect.NotNil(zvFunc) })
	expect.NotNil(io.EOF)
	// expect.Failure(func() { expect.NotNil(zvInterface) })
	expect.NotNil(make(map[string]int))
	expect.Failure(func() { expect.NotNil(zvMap) })
	expect.NotNil(t)
	expect.Failure(func() { expect.NotNil(zvPtr) })
	expect.NotNil([]int{})
	expect.Failure(func() { expect.NotNil(zvSlice) })

	// non-nillable types
	expect.NotNil(0)
	expect.NotNil(5i)
	expect.NotNil("hey")
	expect.NotNil(true)
	expect.NotNil(3.14)
	expect.NotNil(struct{}{})
}

func TestNil(t *testing.T) {
	defer expect.Cleanup(t)
	expect.Nil(nil)

	// nillable types
	expect.Nil(zvChan)
	expect.Failure(func() { expect.Nil(make(chan int)) })
	expect.Nil(zvFunc)
	expect.Failure(func() { expect.Nil(func() {}) })
	// expect.Nil(zvInterface)
	expect.Failure(func() { expect.Nil(io.EOF) })
	expect.Nil(zvMap)
	expect.Failure(func() { expect.Nil(make(map[string]int)) })
	expect.Nil(zvPtr)
	expect.Failure(func() { expect.Nil(t) })
	expect.Nil(zvSlice)
	expect.Failure(func() { expect.Nil([]int{}) })

	// non-nillable types
	expect.Failure(func() { expect.Nil(0) })
	expect.Failure(func() { expect.Nil(5i) })
	expect.Failure(func() { expect.Nil("hey") })
	expect.Failure(func() { expect.Nil(true) })
	expect.Failure(func() { expect.Nil(3.14) })
	expect.Failure(func() { expect.Nil(struct{}{}) })
}

func TestNilTrue(t *testing.T) {
	defer expect.Cleanup(t)
	// expect.True(nil == nil)

	// nillable types
	expect.True(nil == zvChan)
	expect.Failure(func() { expect.True(nil == make(chan int)) })
	expect.True(nil == zvFunc)
	expect.Failure(func() { expect.True(nil == func() {}) })
	// expect.True(nil == zvInterface)
	expect.Failure(func() { expect.True(nil == io.EOF) })
	expect.True(nil == zvMap)
	expect.Failure(func() { expect.True(nil == make(map[string]int)) })
	expect.True(nil == zvPtr)
	expect.Failure(func() { expect.True(nil == t) })
	expect.True(nil == zvSlice)
	expect.Failure(func() { expect.True(nil == []int{}) })

	// non-nillable types
	// expect.Failure(func() { expect.True(nil == 0) })
	// expect.Failure(func() { expect.True(nil == 5i) })
	// expect.Failure(func() { expect.True(nil == "hey") })
	// expect.Failure(func() { expect.True(nil == true) })
	// expect.Failure(func() { expect.True(nil == 3.14) })
	// expect.Failure(func() { expect.True(nil == struct{}{}) })
}

func TestZero(t *testing.T) {
	defer expect.Cleanup(t)

	expect.Failure(func() { expect.Zero(nil) })

	// nillable types
	expect.Zero(zvChan)
	expect.Failure(func() { expect.Zero(make(chan int)) })
	expect.Zero(zvFunc)
	expect.Failure(func() { expect.Zero(func() {}) })
	// expect.Zero(zvInterface)
	// expect.Failure(func() { expect.Zero(io.EOF) })
	expect.Zero(zvMap)
	expect.Failure(func() { expect.Zero(make(map[string]int)) })
	expect.Zero(zvPtr)
	expect.Failure(func() { expect.Zero(t) })
	expect.Zero(zvSlice)
	expect.Failure(func() { expect.Zero([]int{}) })

	// non-nillable types
	expect.Zero(0)
	expect.Failure(func() { expect.Zero(1) })
	expect.Zero(0i)
	expect.Failure(func() { expect.Zero(1i) })
	expect.Zero("")
	expect.Failure(func() { expect.Zero("1") })
	expect.Zero(false)
	expect.Failure(func() { expect.Zero(true) })
	expect.Zero(0.0)
	expect.Failure(func() { expect.Zero(1.0) })
	expect.Zero(struct{ int }{})
	expect.Failure(func() { expect.Zero(struct{ int }{1}) })
}

func TestNotZero(t *testing.T) {
	defer expect.Cleanup(t)

	expect.Failure(func() { expect.NotZero(nil) })

	// nillable types
	expect.Failure(func() { expect.NotZero(zvChan) })
	expect.NotZero(make(chan int))
	expect.Failure(func() { expect.NotZero(zvFunc) })
	expect.NotZero(func() {})
	// expect.Failure(func() { expect.Zero(zvInterface) })
	// expect.NotZero(io.EOF)
	expect.Failure(func() { expect.NotZero(zvMap) })
	expect.NotZero(make(map[string]int))
	expect.Failure(func() { expect.NotZero(zvPtr) })
	expect.NotZero(t)
	expect.Failure(func() { expect.NotZero(zvSlice) })
	expect.NotZero([]int{})

	// non-nillable types
	expect.Zero(0)
	expect.Failure(func() { expect.Zero(1) })
	expect.Zero(0i)
	expect.Failure(func() { expect.Zero(1i) })
	expect.Zero("")
	expect.Failure(func() { expect.Zero("1") })
	expect.Zero(false)
	expect.Failure(func() { expect.Zero(true) })
	expect.Zero(0.0)
	expect.Failure(func() { expect.Zero(1.0) })
	expect.Zero(struct{ int }{})
	expect.Failure(func() { expect.Zero(struct{ int }{1}) })
}
