/* Package expectlib implements low-level expect operations.

The functions are mostly here for implementation details, but still available for external use.
*/
package expectlib

import (
	"errors"
	"fmt"
	"runtime"
)

// InternalError represents a basic expectlib error, mainly panicked with.
type InternalError struct {
	// The message you want to have for your error
	Msg string
	// The call stack as returned from runtime.Callers
	// You then must call runtime.CallersFrames(ie.Stack) to have useful info
	// This keeps the performance footprint way down
	Stack []uintptr
}

// Error implements the error interface.
func (ie InternalError) Error() string {
	return ie.Msg
}

// AsInternalError returns v as an InternalError, with wrapping taken into account.
func AsInternalError(i interface{}) (ie InternalError, ok bool) {
	var err error
	err, ok = i.(error)
	if !ok {
		return
	}

	ok = errors.As(err, &ie)
	return
}

// InternalFailf is equivalent to InternalFail(skip+1, fmt.Sprintf(format, a...))
func InternalFailf(skip int, format string, a ...interface{}) {
	InternalFail(skip+1, fmt.Sprintf(format, a...))
}

// InternalFail creates an InternalError with its call stack (skipping skip+1 frames, use
// 0 for check implementations), and then panics with InternalError.
func InternalFail(skip int, message string) {
	// do not under-allocate, Callers call is expensive
	// but do not over-allocate, GC pressure is also expensive (still way less than Callers call)
	// 64 seems to be the best with benchmarks showing performance is not too bad for shallow stacks
	// (~10 depth) but still quite good for deep stacks (30+)
	const frames = 64
	rpc := make([]uintptr, frames)
	// 1 for Callers semantics, +1 for skipping InternalFail itself, +1 for skipping its caller
	const skipFrames = 3
	n := runtime.Callers(skipFrames+skip, rpc)
	rpc = rpc[:n]

	// take more frames if needed, is not performant but correct
	if n == frames {
		// double frames read each iteration to attain correct value faster
		tframes := 64
		for n == tframes {
			tframes *= 2
			tmp := make([]uintptr, tframes)
			n = runtime.Callers(skipFrames+len(rpc)+skip, tmp)
			rpc = append(rpc, tmp[:n]...)
		}
	}

	err := InternalError{
		Msg:   message,
		Stack: rpc,
	}
	panic(err)
}
